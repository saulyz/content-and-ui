
let collapsibleLists = document.querySelectorAll('.list-collapsible');
collapsibleLists.forEach(function(item){
  console.log(item);
  createListCollapsibleControl(item);
});

function createListCollapsibleControl(list) {
  const maxListSize = 3;
  let listSize = list.children.length;
  console.log(listSize);

  if (listSize > maxListSize) {
    let control = document.createElement('div');
    control.setAttribute('tabindex', 0);
    control.setAttribute('role', 'button');
    
    let more = document.createElement('span');
    more.classList.add('more', 'button-link');
    more.innerText = `Show more (${listSize - maxListSize})`;
    let iconDown = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M7.41 8.59L12 13.17l4.59-4.58L18 10l-6 6-6-6 1.41-1.41z"/><path fill="none" d="M0 0h24v24H0V0z"/></svg>';
    more.insertAdjacentHTML('beforeend', iconDown);
    
    let less = document.createElement('span');
    let iconUp = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6z"/><path d="M0 0h24v24H0z" fill="none"/></svg>';
    less.classList.add('less', 'button-link');
    less.innerText = 'Show less';
    less.insertAdjacentHTML('beforeend', iconUp);
    
    control.appendChild(more);
    control.appendChild(less);
    
    list.parentNode.insertBefore(control, list.nextSibling);
  
    list.classList.add('show-' + maxListSize);
    control.addEventListener('click', controlHandler);
    control.addEventListener('keydown', controlHandler);
  }
  
  function controlHandler(event) {
    list.classList.toggle('show-' + maxListSize);
  }
}
